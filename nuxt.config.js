export default {
    plugins: ['~plugins/vuetify.js'],

    css: [
        {
            src: '../assets/css/app.styl',
            lang: 'styl'
        }
    ],

    build: {
        vendor: ['vuetify'],
    },
    modules: ['nuxt-material-design-icons'],

    head: {
        link: [
            { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:100' }
        ]
    }

}